<?php
/**
 * Created by PhpStorm.
 * User: Ovi
 * Date: 1/29/2017
 * Time: 1:51 AM
 */

namespace App\Summary_Of_Organization;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
use PDO;

class Summary_Of_Organization extends DB
{
    private $id;
    private $name;
    private $summary_of_organization;
    private $organization_name;

    public function setData($allPostData=null){
        if(array_key_exists("id",$allPostData)){
            $this -> id = $allPostData['id'];
        }
        if(array_key_exists("user_name",$allPostData)){
            $this -> name = $allPostData['user_name'];
        }
        if(array_key_exists("organization_name",$allPostData)){
            $this -> organization_name = $allPostData['organization_name'];
        }
        if(array_key_exists("summary_of_organization",$allPostData)){
            $this -> summary_of_organization = $allPostData['summary_of_organization'];
        }
    }
    public function store(){
        $arrayData = array($this-> name,$this->organization_name,$this->summary_of_organization);
        $query = 'INSERT INTO summary_of_organization (user_name, organization_name,summary_of_organization) VALUES (?,?,?)';

        $STH = $this->DBH->prepare($query);
        $result = $STH-> execute($arrayData);

        if($result){
            Message::setMessage("Success! Data has been inserted successfully!");
        }
        else{
            Message::setMessage("Failed! Data has not been stored!");
        }
        Utility::redirect('index.php');
    }
    public function index()
    {

        $sql = "SELECT * from summary_of_organization";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();


    }
    public function view()
    {

        $sql = "SELECT * from summary_of_organization WHERE user_id=".$this->id;
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();


    }

}