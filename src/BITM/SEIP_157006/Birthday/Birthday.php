<?php
/**
 * Created by PhpStorm.
 * User: Ovi
 * Date: 1/28/2017
 * Time: 11:41 PM
 */

namespace App\Birthday;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
use PDO;

class Birthday extends DB
{
    private $id;
    private $name;
    private $birthday;

    public function setData($allPostData=null){
        if(array_key_exists("id",$allPostData)){
            $this -> id = $allPostData['id'];
        }
        if(array_key_exists("user_name",$allPostData)){
            $this -> name = $allPostData['user_name'];
        }
        if(array_key_exists("birthday",$allPostData)){
            $this -> birthday = $allPostData['birthday'];
        }
    }
    public function store(){
        $arrayData = array($this-> name,$this->birthday);
        $query = 'INSERT INTO birthday (user_name, birthday) VALUES (?,?)';

        $STH = $this->DBH->prepare($query);
        $result = $STH-> execute($arrayData);

        if($result){
            Message::setMessage("Success! Data has been inserted successfully!");
        }
        else{
            Message::setMessage("Failed! Data has not been stored!");
        }
        Utility::redirect('index.php');
    }
    public function index()
    {

        $sql = "SELECT * from birthday";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();


    }
    public function view()
    {

        $sql = "SELECT * from birthday WHERE user_id=".$this->id;
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();


    }
}