<?php
/**
 * Created by PhpStorm.
 * User: Ovi
 * Date: 1/29/2017
 * Time: 1:30 AM
 */

namespace App\Gender;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
use PDO;

class Gender extends DB
{

    private $id;
    private $name;
    private $gender;

    public function setData($allPostData=null){
        if(array_key_exists("id",$allPostData)){
            $this-> id = $allPostData['id'];
        }
        if(array_key_exists("user_name",$allPostData)){
            $this-> name = $allPostData['user_name'];
        }
        if(array_key_exists("gender",$allPostData)){
            $this->gender = $allPostData['gender'];
        }
    }
    public function store(){
        $arrayData = array($this-> name,$this->gender);
        $query = 'INSERT INTO gender (user_name, gender) VALUES (?,?)';

        $STH = $this->DBH->prepare($query);
        $result = $STH-> execute($arrayData);

        if($result){
            Message::setMessage("Success! Data has been inserted successfully!");
        }
        else{
            Message::setMessage("Failed! Data has not been stored!");
        }
        Utility::redirect('index.php');
    }
    public function index()
    {

        $sql = "SELECT * from gender ";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();


    }
    public function view()
    {

        $sql = "SELECT * from gender WHERE user_id=".$this->id;
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();


    }
}