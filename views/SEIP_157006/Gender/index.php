<?php
require_once("../../../vendor/autoload.php");

$objGender = new \App\Gender\Gender();
$allData = $objGender-> index();






use App\Message\Message;
if(!isset($_SESSION))
{
    session_start();
}

$msg = Message::getMessage();
echo "<div id= 'message'> $msg </div>";



?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Gender view</title>
    <style>
        table{
            border: 1px;

        }
        td{
            border: 0px;
        }
    </style>
</head>
<body>
<h1>Gender</h1>
<table cellspacing="0px" class="table table-stried">
    <tr>
        <th>Serial Number</th>
        <th>ID</th>
        <th>Name</th>
        <th>Gender</th>
        <th>Action Buttons</th>
    </tr>
    <?php
    $serial=1;
    foreach($allData as $oneData){
        if($serial%2){
            $bgColor = '#cccccc';
        }
        else
            $bgColor = '#ffffff';
        echo "
                <tr style='background-color: $bgColor'>
                    <td>$serial</td>
                    <td>$oneData->user_id</td>
                    <td>$oneData->user_name</td>
                    <td>$oneData->gender</td>
                    <td><a href='view.php?id=$oneData->user_id' class='btn btn-info'>View</a></td>

                </tr>
            ";
        $serial++;

    }


    ?>
</table>
</body>
</html>